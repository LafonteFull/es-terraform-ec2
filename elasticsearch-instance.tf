resource "aws_instance" "elasticsearch_instance" {
  ami           = "${var.aws_ami}"
  instance_type = "${var.aws_instance_type}"
  key_name = "${var.ssh_key_name}"
  security_groups = ["${aws_security_group.elasticsearch_sg.name}"]
  iam_instance_profile = "${aws_iam_instance_profile.elasticsearch_profile.name}"
  ebs_block_device {
    device_name = "${var.ebs_device_name}"
    volume_type = "${var.ebs_volume_type}"
    volume_size = "${var.ebs_volume_size}"
  }

  provisioner "file" {
    source      = "${var.exec_file_name}"
    destination = "/tmp/${var.exec_file_name}"
  }
  provisioner "file" {
    source      = ".java.policy"
    destination = "/home/admin/.java.policy"
  }
  provisioner "file" {
    source      = "certificate-bundle.zip"
    destination = "/home/admin/certificate-bundle.zip"
  }
  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/${var.exec_file_name}",
      "/tmp/${var.exec_file_name}",
    ]
  }
  count = "${var.node_count}"

  tags = {
    Name = "elasticsearch_instance_${count.index}"
    ec2discovery = "elk"
  }

  # Login to the instance with the aws key.
  connection {
    type        = "ssh"
    user        = "admin"
    password    = ""
    private_key = file("${var.keyPath}")
    host        = self.public_ip
  }
}