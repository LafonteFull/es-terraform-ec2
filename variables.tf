variable "aws_region" {
  default     = "ap-southeast-1"
}

variable "node_count" {
  default     = "1"
}

variable "aws_access_key" {
  default     = ""
}

variable "aws_secret_key" {
  default     = ""
}

# Debian jessie based image
variable "aws_ami" {
  default = "ami-ac360cfe"
}

variable "aws_instance_type" {
  default     = "t2.micro"
}

variable "ebs_device_name" {
  default     = "/dev/xvdb"
}

variable "ebs_volume_type" {
  default     = "gp2"
}

variable "ebs_volume_size" {
  default     = "8"
}

variable "ssh_key_name" {
  default     = "aryo-test"
}

variable "exec_file_name" {
  default     = "elasticsearch-node-setup.sh"
}

variable "keyPath" {
  default     = "aryo-test.pem"
}