#!/bin/bash

# Script used to set up a new Elasticsearch node in AWS
sudo apt-get update
sudo apt-get install unzip -y

sudo sysctl -w vm.max_map_count=262144

# Java 9 Installation
wget --header "Cookie: oraclelicense=accept-securebackup-cookie" \
    https://download.java.net/java/GA/jdk9/9.0.4/binaries/openjdk-9.0.4_linux-x64_bin.tar.gz

tar xvzf openjdk-9.0.4_linux-x64_bin.tar.gz -C /home/admin
export JAVA_HOME=/home/admin/jdk-9.0.4
export PATH="$PATH:${JAVA_HOME}/bin"

# Elasticsearch 5.2.1 Installation
wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-5.2.1.tar.gz
wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-5.2.1.tar.gz.sha512
https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-5.2.1.deb.sha1
shasum -a 512 -c elasticsearch-5.2.1.tar.gz.sha512
tar -xzf elasticsearch-5.2.1.tar.gz
chown -R admin:admin elasticsearch-5.2.1
export ES_HOME=/home/admin/elasticsearch-5.2.1
export ES_PATH_CONF=$ES_HOME/config

# Update Elasticsearch jvm to 512m
sed -e 's/-Xmx2g/-Xmx512m/; s/-Xms2g/-Xms512m/;' $ES_PATH_CONF/jvm.options > $ES_PATH_CONF/jvm.options.tmp
mv $ES_PATH_CONF/jvm.options.tmp $ES_PATH_CONF/jvm.options

# Discovery EC2 plugin is used for the nodes to create the cluster in AWS
echo "y" | $ES_HOME/bin/elasticsearch-plugin install discovery-ec2

# setup credential and encryption
echo "y" | $ES_HOME/bin/elasticsearch-plugin install x-pack
mkdir $ES_HOME/config/x-pack/certs
unzip /home/admin/certificate-bundle.zip -d $ES_HOME/config/x-pack/certs

# Shortest configuration for Elasticsearch
echo "discovery.zen.hosts_provider: ec2" >> $ES_HOME/config/elasticsearch.yml
echo "cloud.aws.region: ap-southeast" >> $ES_HOME/config/elasticsearch.yml
echo "network.host: _ec2_" >> $ES_HOME/config/elasticsearch.yml
echo "xpack.ssl.key:                     /home/admin/elasticsearch-5.2.1/config/x-pack/certs/ca/ca.key" >> $ES_HOME/config/elasticsearch.yml
echo "xpack.ssl.certificate:             /home/admin/elasticsearch-5.2.1/config/x-pack/certs/ca/ca.crt" >> $ES_HOME/config/elasticsearch.yml
echo 'xpack.ssl.certificate_authorities: [ "/home/admin/elasticsearch-5.2.1/config/x-pack/certs/ca/ca.crt" ]' >> $ES_HOME/config/elasticsearch.yml
echo "xpack.security.transport.ssl.enabled: true" >> $ES_HOME/config/elasticsearch.yml
echo "xpack.security.http.ssl.enabled: true" >> $ES_HOME/config/elasticsearch.yml

# Run Elasticsearch
$ES_HOME/bin/elasticsearch

echo "Node setup finished!" > ~/terraform.txt